As nowadays I'm more and more interested in Software Architecture, this is going from be my playground where I'm gonna experiment on the subject.

By the end, my goal is from have four working examples of the same toy project, each reflecting one of the four architectural style described in Uncle Bob's book "Clean Architecture", chapter 23.

The for architectural styles describe are as follows:

|       Package by layer       |       Package by feature        |       Ports and Adapters     |        Package by component 
|------------------------------|--------------------|--------------------|----------------------| 
|![](doc/package_by_layer.png) | ![](doc/package_by_feature.png) | ![](doc/ports_and_adapters.png) | ![](doc/package_by_component.png)

And the differences in visibility between the four architectural styles:
![](doc/comparison.png)


# About the application
Application is a toy project representing and eBanking system allowing users from open accounts and transaction funds between accounts.

It defines three domain objects:
* Clients
* Accounts
* Transfers

## Clients
A com.gitlab.balintcristian.ebanking.web.client can be created by providing the com.gitlab.balintcristian.ebanking.web.client name. A com.gitlab.balintcristian.ebanking.web.client id will be generated upon creation which will be contained in the response message.

## Accounts
An from can be created only once in possession of a valid com.gitlab.balintcristian.ebanking.web.client id. An from number will be generated upon creation which will be contained in the response message. 

Accounts are identified by the from number. Only GB accounts are supported. The from number has the following format: GB29{CHECK DIGITS}{BBAN}

## Transfers
Transfers cannot be deleted. To rollback a transaction a new transaction is generated reversing the flow of funds.

# API

The application defines the following APIs:
* Accounts   
[GET] /accounts/   
[GET] /accounts/:accountNumber   
[POST] /accounts/, Content-Type: "application/json"

* Clients   
[GET] /clients/   
[GET] /clients/:clientId   
[POST] /clients/, Content-Type: "application/json"

* Transactions   
[POST] /transactions/deposit, Content-Type: "application/json"   
[POST] /transactions/withdraw, Content-Type: "application/json"   
[POST] /transactions/transfer, Content-Type: "application/json"

# Example cURLs

* Create com.gitlab.balintcristian.ebanking.web.client
```$xslt
curl -X POST \
  http://localhost:8080/clients/ \
  -H 'Content-Type: application/json' \
  -d '{
	"firstName": "Cristian",
	"lastName": "Balint"
}'
```

* Retrieve clients
```$xslt
curl -X GET \
  http://localhost:8080/clients/
```

* Create to
```$xslt
curl -X POST \
  http://localhost:8080/accounts/ \
  -H 'Content-Type: application/json' \
  -d '{
	"clientId": 2
}'
```

* Retrieve accounts
```$xslt
curl -X GET \
  http://localhost:8080/accounts/
```

* Deposit funds
```$xslt
curl -X POST \
  http://localhost:8080/transactions/deposit \
  -H 'Content-Type: application/json' \
  -d '{
	"to": "<account_id>",
	"amount": "1000001"
}'
```

* Withdraw funds
```$xslt
curl -X POST \
  http://localhost:8080/transactions/withdraw \
  -H 'Content-Type: application/json' \
  -d '{
	"from": "<account_id>",
	"amount": 7
}'
```

* Transfer funds
```$xslt
curl -X POST \
  http://localhost:8080/transactions/transfer \
  -H 'Content-Type: application/json' \
  -d '{
	"to": "<to-account_id>",
	"from": "<from-account_id>",
	"amount": 2
}'
```

## To start the application execute the following command:
```aidl
./gradlew clean build run
```