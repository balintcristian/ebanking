package com.gitlab.balintcristian.ebanking.web.account;

import com.gitlab.balintcristian.ebanking.web.transaction.Transaction;
import lombok.*;

import java.util.List;

@Getter
@Setter
@ToString
public class Account {
    private Client owner;
    private String number;
    private String balance;
    private List<Transaction> transactionHistory;

    @Getter
    @Setter
    final static class Client {
        private String id;
        private String firstName;
        private String lastName;
    }
}
