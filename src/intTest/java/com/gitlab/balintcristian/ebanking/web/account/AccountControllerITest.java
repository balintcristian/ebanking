package com.gitlab.balintcristian.ebanking.web.account;

import com.gitlab.balintcristian.ebanking.Application;
import com.gitlab.balintcristian.ebanking.core.account.AccountRepository;
import com.gitlab.balintcristian.ebanking.core.client.ClientRepository;
import com.gitlab.balintcristian.ebanking.web.IntTestBase;
import org.junit.Test;

import javax.ws.rs.client.Entity;
import java.util.List;

import static java.util.Collections.emptyList;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.startsWith;
import static org.hamcrest.MatcherAssert.assertThat;

public class AccountControllerITest extends IntTestBase {

    @Test
    public void shouldCreateAccount() {
        ClientRepository clientRepository = Application.persistenceConfiguration().clientRepository();
        clientRepository.save(new com.gitlab.balintcristian.ebanking.core.client.Client(
                "1",
                "John",
                "Doe"
        ));

        Account account = target
                .path("/accounts/")
                .request()
                .header("Accept", "application/json")
                .header("Content-Type", "application/json")
                .post(Entity.json("" +
                        "{" +
                        "\"clientId\": \"1\"" +
                        "}"), Account.class);

        assertThat(account.getBalance(), is("0"));
        assertThat(account.getNumber(), startsWith("GB29"));
        assertThat(account.getOwner().getId(), is("1"));
        assertThat(account.getOwner().getLastName(), is("Doe"));
        assertThat(account.getOwner().getFirstName(), is("John"));
    }

    @Test
    public void shouldRetrieveAccount() {
        ClientRepository clientRepository = Application.persistenceConfiguration().clientRepository();
        clientRepository.save(new com.gitlab.balintcristian.ebanking.core.client.Client(
                "1",
                "John",
                "Doe"
        ));
        AccountRepository accountRepository = Application.persistenceConfiguration().accountRepository();
        accountRepository.save(new com.gitlab.balintcristian.ebanking.core.account.Account(
                "1",
                "GB29123"
        ));

        Account account = target
                .path("/accounts/GB29123")
                .request()
                .header("Accept", "application/json")
                .get(Account.class);

        assertThat(account.getBalance(), is("0"));
        assertThat(account.getNumber(), startsWith("GB29123"));
        assertThat(account.getOwner().getId(), is("1"));
        assertThat(account.getOwner().getLastName(), is("Doe"));
        assertThat(account.getOwner().getFirstName(), is("John"));
    }

    @Test
    public void shouldRetrieveAllAccounts() {
        ClientRepository clientRepository = Application.persistenceConfiguration().clientRepository();
        clientRepository.save(new com.gitlab.balintcristian.ebanking.core.client.Client(
                "1",
                "John",
                "Doe"
        ));
        AccountRepository accountRepository = Application.persistenceConfiguration().accountRepository();
        accountRepository.save(new com.gitlab.balintcristian.ebanking.core.account.Account(
                "1",
                "GB29123"
        ));
        accountRepository.save(new com.gitlab.balintcristian.ebanking.core.account.Account(
                "1",
                "GB29321"
        ));

        List accounts = target
                .path("/accounts/")
                .request()
                .header("Accept", "application/json")
                .get(List.class);

        assertThat(accounts.size(), is(2));

        Account account1 = GSON.fromJson(GSON.toJson(accounts.get(0)), Account.class);
        assertThat(account1.getNumber(), is("GB29123"));
        assertThat(account1.getBalance(), is("0"));

        Account account2 = GSON.fromJson(GSON.toJson(accounts.get(1)), Account.class);
        assertThat(account2.getNumber(), is("GB29321"));
        assertThat(account2.getBalance(), is("0"));
        assertThat(account2.getTransactionHistory(), is(emptyList()));
    }
}
