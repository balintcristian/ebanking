package com.gitlab.balintcristian.ebanking.web.client;

import lombok.*;
import lombok.experimental.Accessors;

import java.util.List;

@Getter
@Setter
@ToString
public class Client {
    private String id;
    private String firstName;
    private String lastName;
    private List<Account> accounts;

    @Getter
    @Setter
    final static class Account {
        private String number;
        private String balance;
    }
}
