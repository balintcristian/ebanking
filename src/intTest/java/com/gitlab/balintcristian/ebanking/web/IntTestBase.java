package com.gitlab.balintcristian.ebanking.web;

import com.gitlab.balintcristian.ebanking.Application;
import com.google.gson.Gson;
import org.junit.After;
import org.junit.Before;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

public abstract class IntTestBase {
    protected static final Gson GSON = new Gson();

    protected WebTarget target;

    @Before
    public void before() throws InterruptedException {
        Application.start();
        Thread.sleep(100);
        target = ClientBuilder.newClient().target("http://localhost:8080/");
    }

    @After
    public void tearDown() throws InterruptedException {
        Application.stop();
        Thread.sleep(100);
    }
}