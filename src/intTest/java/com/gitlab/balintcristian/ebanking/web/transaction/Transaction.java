package com.gitlab.balintcristian.ebanking.web.transaction;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Transaction {
    private String id;
    private String amount;
}

