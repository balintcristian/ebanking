package com.gitlab.balintcristian.ebanking.web.safety;

import com.gitlab.balintcristian.ebanking.Application;
import com.gitlab.balintcristian.ebanking.core.account.Account;
import com.gitlab.balintcristian.ebanking.core.account.AccountRepository;
import com.gitlab.balintcristian.ebanking.core.client.ClientRepository;
import com.gitlab.balintcristian.ebanking.web.IntTestBase;
import com.gitlab.balintcristian.ebanking.web.transaction.Transaction;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import javax.ws.rs.client.Entity;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@Slf4j
public class ThreadSafeITest extends IntTestBase {

    @Test
    public void shouldBeThreadSafe() {
        ClientRepository clientRepository = Application.persistenceConfiguration().clientRepository();
        AccountRepository accountRepository = Application.persistenceConfiguration().accountRepository();

        clientRepository.save(new com.gitlab.balintcristian.ebanking.core.client.Client(
                "1",
                "John",
                "Doe"
        ));
        Account joeAccount1 = new Account("1", "GB29123");
        Account joeAccount2 = new Account("1", "GB29321");
        joeAccount1.deposit(new BigInteger("100000"));
        joeAccount2.deposit(new BigInteger("100000"));
        accountRepository.save(joeAccount1);
        accountRepository.save(joeAccount2);

        clientRepository.save(new com.gitlab.balintcristian.ebanking.core.client.Client(
                "2",
                "Jane",
                "Roe"
        ));
        Account janeAccount1 = new Account("2", "GB29456");
        Account janeAccount2 = new Account("2", "GB29654");
        Account janeAccount3 = new Account("2", "GB29645");
        janeAccount1.deposit(new BigInteger("100000"));
        janeAccount2.deposit(new BigInteger("100000"));
        janeAccount3.deposit(new BigInteger("100000"));
        accountRepository.save(janeAccount1);
        accountRepository.save(janeAccount2);
        accountRepository.save(janeAccount3);

        final List<Account> accounts = List.of(joeAccount1, joeAccount2, janeAccount1, janeAccount2, janeAccount3);

        ForkJoinPool pool = ForkJoinPool.commonPool();
        List<ForkJoinTask<?>> futures = new ArrayList<>();

        int tasks = Runtime.getRuntime().availableProcessors() * 8 + 1;

        for (int t = 0; t < tasks; t++) {
            ForkJoinTask<?> f = pool.submit(() -> {
                Random random = new Random();

                for (int i = 0; i <100; i++){

                    int sourceId = random.nextInt(5);
                    int destId;
                    do {
                        destId = random.nextInt(5);
                    } while (destId == sourceId);

                    int amount = random.nextInt(100) + 1;

                    Transaction result = target
                            .path("/transactions/transfer")
                            .request()
                            .header("Accept", "application/json")
                            .post(Entity.json("" +
                                    "{" +
                                    "\"to\": \"" + accounts.get(destId).number() + "\"," +
                                    "\"from\": \"" + accounts.get(sourceId).number() + "\"," +
                                    "\"amount\": \"" + amount + "\"" +
                                    "}"), Transaction.class);
                    log.info("Transaction '{}' finished.", result.getId());
                }
            });

            futures.add(f);
        }

        futures.forEach(ForkJoinTask::join);

        BigInteger sum = accounts.stream()
                .map(Account::balance)
                .reduce(BigInteger::add)
                .get();

        assertThat(sum, is(new BigInteger("500000")));
    }
}
