package com.gitlab.balintcristian.ebanking.web.transaction;

import com.gitlab.balintcristian.ebanking.Application;
import com.gitlab.balintcristian.ebanking.core.account.Account;
import com.gitlab.balintcristian.ebanking.core.account.AccountRepository;
import com.gitlab.balintcristian.ebanking.core.client.ClientRepository;
import com.gitlab.balintcristian.ebanking.web.IntTestBase;
import org.junit.Test;

import javax.ws.rs.client.Entity;

import java.math.BigInteger;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class TransactionControllerITest extends IntTestBase {

    @Test
    public void shouldDepositFunds() {
        ClientRepository clientRepository = Application.persistenceConfiguration().clientRepository();
        clientRepository.save(new com.gitlab.balintcristian.ebanking.core.client.Client(
                "1",
                "John",
                "Doe"
        ));
        AccountRepository accountRepository = Application.persistenceConfiguration().accountRepository();
        accountRepository.save(new com.gitlab.balintcristian.ebanking.core.account.Account(
                "1",
                "GB29123"
        ));
        Account account = accountRepository.get("GB29123");
        assertThat(account.balance().toString(), is("0"));

        Transaction transaction = target
                .path("/transactions/deposit")
                .request()
                .header("Accept", "application/json")
                .post(Entity.json("" +
                        "{" +
                        "\"to\": \"GB29123\"," +
                        "\"amount\": \"100\"" +
                        "}"), Transaction.class);

        account = accountRepository.get("GB29123");
        assertThat(account.balance().toString(), is("100"));
        assertThat(transaction.getAmount(), is("100"));
    }

    @Test
    public void shouldWithdrawFunds() {
        ClientRepository clientRepository = Application.persistenceConfiguration().clientRepository();
        clientRepository.save(new com.gitlab.balintcristian.ebanking.core.client.Client(
                "1",
                "John",
                "Doe"
        ));
        AccountRepository accountRepository = Application.persistenceConfiguration().accountRepository();
        Account account = new Account("1", "GB29123");
        account.deposit(BigInteger.TEN);
        accountRepository.save(account);
        assertThat(account.balance().toString(), is("10"));

        Transaction transaction = target
                .path("/transactions/withdraw")
                .request()
                .header("Accept", "application/json")
                .post(Entity.json("" +
                        "{" +
                        "\"from\": \"GB29123\"," +
                        "\"amount\": \"1\"" +
                        "}"), Transaction.class);

        account = accountRepository.get("GB29123");
        assertThat(account.balance().toString(), is("9"));
        assertThat(transaction.getAmount(), is("-1"));
    }

    @Test
    public void shouldTransferFunds() {
        ClientRepository clientRepository = Application.persistenceConfiguration().clientRepository();
        clientRepository.save(new com.gitlab.balintcristian.ebanking.core.client.Client(
                "1",
                "John",
                "Doe"
        ));
        AccountRepository accountRepository = Application.persistenceConfiguration().accountRepository();
        Account accountTo = new Account("1", "GB29321");
        Account accountFrom = new Account("1", "GB29123");
        accountFrom.deposit(BigInteger.TEN);
        accountRepository.save(accountFrom);
        accountRepository.save(accountTo);
        assertThat(accountTo.balance().toString(), is("0"));
        assertThat(accountFrom.balance().toString(), is("10"));

        Transaction transaction = target
                .path("/transactions/transfer")
                .request()
                .header("Accept", "application/json")
                .post(Entity.json("" +
                        "{" +
                        "\"to\": \"GB29321\"," +
                        "\"from\": \"GB29123\"," +
                        "\"amount\": \"1\"" +
                        "}"), Transaction.class);

        accountTo = accountRepository.get("GB29321");
        accountFrom = accountRepository.get("GB29123");
        assertThat(transaction.getAmount(), is("1"));
        assertThat(accountTo.balance().toString(), is("1"));
        assertThat(accountFrom.balance().toString(), is("9"));
    }
}
