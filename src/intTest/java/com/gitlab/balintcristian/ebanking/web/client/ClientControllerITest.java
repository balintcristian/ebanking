package com.gitlab.balintcristian.ebanking.web.client;

import com.gitlab.balintcristian.ebanking.Application;
import com.gitlab.balintcristian.ebanking.core.client.ClientRepository;
import com.gitlab.balintcristian.ebanking.web.IntTestBase;
import org.junit.Test;

import javax.ws.rs.client.Entity;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class ClientControllerITest extends IntTestBase {

    @Test
    public void shouldCreateClient() {

        Client client = target
                .path("/clients/")
                .request()
                .header("Accept", "application/json")
                .header("Content-Type", "application/json")
                .post(Entity.json("" +
                        "{" +
                        "\"firstName\": \"John\"," +
                        "\"lastName\": \"Doe\"" +
                        "}"), Client.class);

        assertThat(client.getLastName(), is("Doe"));
        assertThat(client.getFirstName(), is("John"));
    }

    @Test
    public void shouldRetrieveClient() {

        ClientRepository clientRepository = Application.persistenceConfiguration().clientRepository();
        clientRepository.save(new com.gitlab.balintcristian.ebanking.core.client.Client(
                "1",
                "John",
                "Doe"
        ));

        Client client = target
                .path("/clients/1")
                .request()
                .header("Accept", "application/json")
                .get(Client.class);

        assertThat(client.getId(), is("1"));
        assertThat(client.getLastName(), is("Doe"));
        assertThat(client.getFirstName(), is("John"));
    }

    @Test
    public void shouldRetrieveAllClients() {

        ClientRepository clientRepository = Application.persistenceConfiguration().clientRepository();
        clientRepository.save(new com.gitlab.balintcristian.ebanking.core.client.Client(
                "1",
                "John",
                "Doe"
        ));
        clientRepository.save(new com.gitlab.balintcristian.ebanking.core.client.Client(
                "2",
                "Jane",
                "Roe"
        ));

        List clients = target
                .path("/clients/")
                .request()
                .header("Accept", "application/json")
                .get(List.class);

        assertThat(clients.size(), is(2));

        Client john = GSON.fromJson(GSON.toJson(clients.get(0)), Client.class);
        assertThat(john.getId(), is("1"));
        assertThat(john.getLastName(), is("Doe"));
        assertThat(john.getFirstName(), is("John"));

        Client jane = GSON.fromJson(GSON.toJson(clients.get(1)), Client.class);
        assertThat(jane.getId(), is("2"));
        assertThat(jane.getLastName(), is("Roe"));
        assertThat(jane.getFirstName(), is("Jane"));
    }
}
