package com.gitlab.balintcristian.ebanking.core.account;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.startsWith;
import static org.hamcrest.MatcherAssert.assertThat;

public class GbAccountNumberGeneratorTest {

    private static final int COUNTRY_CODE_LENGTH = 2;
    private static final int CHECK_DIGITS_LENGTH = 2;

    private GbAccountNumberGenerator gbAccountNumberGenerator;

    @Before
    public void setUp() {
        gbAccountNumberGenerator = new GbAccountNumberGenerator();
    }

    @Test
    public void shouldGenerateAccountNumbersOfLength14() {
        String accountNo = gbAccountNumberGenerator.next();

        assertThat(accountNo.length(), is(COUNTRY_CODE_LENGTH + CHECK_DIGITS_LENGTH + 14));
    }

    @Test
    public void countryCodeShouldBeGB() {
        String accountNo = gbAccountNumberGenerator.next();

        assertThat(accountNo, startsWith("GB"));
    }

    @Test
    public void checkDigitsShouldBe29() {
        String accountNo = gbAccountNumberGenerator.next();

        assertThat(accountNo.substring(2), startsWith("29"));
    }
}