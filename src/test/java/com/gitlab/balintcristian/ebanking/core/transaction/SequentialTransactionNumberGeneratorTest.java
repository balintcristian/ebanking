package com.gitlab.balintcristian.ebanking.core.transaction;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class SequentialTransactionNumberGeneratorTest {

    SequentialTransactionNumberGenerator generator;

    @Before
    public void setUp() throws Exception {
        generator = new SequentialTransactionNumberGenerator();
    }

    @Test
    public void next() {
        assertEquals(generator.next(), String.valueOf(1L));
        assertEquals(generator.next(), String.valueOf(2L));
        assertEquals(generator.next(), String.valueOf(3L));
        assertEquals(generator.next(), String.valueOf(4L));
    }
}