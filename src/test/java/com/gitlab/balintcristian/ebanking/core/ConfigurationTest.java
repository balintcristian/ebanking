package com.gitlab.balintcristian.ebanking.core;

import com.gitlab.balintcristian.ebanking.core.account.AccountRepository;
import com.gitlab.balintcristian.ebanking.core.account.AccountService;
import com.gitlab.balintcristian.ebanking.core.client.ClientRepository;
import com.gitlab.balintcristian.ebanking.core.client.ClientService;
import com.gitlab.balintcristian.ebanking.core.util.IdGenerator;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

public class ConfigurationTest {

    @Mock
    ClientRepository clientRepository;
    @Mock
    AccountRepository accountRepository;

    Configuration configuration;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        IdGenerator idGenerator = () -> "1";
        configuration = new Configuration(idGenerator, clientRepository, accountRepository);
    }

    @Test
    public void shouldReturnAccountService() {

        AccountService accountService = configuration.accountService();
        assertThat(accountService, notNullValue());
    }

    @Test
    public void shouldReturnClientService() {

        ClientService clientService = configuration.clientService();
        assertThat(clientService, notNullValue());
    }
}