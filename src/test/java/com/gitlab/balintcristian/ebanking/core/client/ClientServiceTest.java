package com.gitlab.balintcristian.ebanking.core.client;

import com.gitlab.balintcristian.ebanking.core.util.IdGenerator;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ClientServiceTest {
    @Mock
    private IdGenerator idGenerator;
    @Mock
    private ClientRepository clientRepository;

    private ClientService clientService;

    private ArgumentCaptor<Client> clientCaptor;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        clientCaptor = ArgumentCaptor.forClass(Client.class);

        clientService = new ClientService(idGenerator, clientRepository);
    }

    @Test
    public void shouldCreateClient() {
        String firstName = "John";
        String lastName = "Doe";

        Client client = clientService.createClient(firstName, lastName);

        assertThat(client, notNullValue());
    }

    @Test
    public void shouldSaveClient() {
        when(idGenerator.nextId()).thenReturn("1");

        clientService.createClient("John", "Doe");

        verify(clientRepository).save(clientCaptor.capture());
        Client client = clientCaptor.getValue();
        assertThat(client.id(), is("1"));
        assertThat(client.lastName(), is("Doe"));
        assertThat(client.firstName(), is("John"));
    }
}