package com.gitlab.balintcristian.ebanking.core.account;

import org.junit.Test;

import java.math.BigInteger;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class AccountTest {

    @Test
    public void shouldBeEqual() {
        Account account1 = new Account("1", "1");
        Account account2 = new Account("2", "1");
        Account account3 = new Account("2", "2");
        Object object = new Object();

        assertEquals(account1, account2);
        assertNotEquals(account1, account3);
        assertNotEquals(account1, object);
    }

    @Test
    public void shouldDepositAmount() {
        Account account = new Account("1", "1");
        BigInteger amount = BigInteger.ONE;

        account.deposit(amount);

        assertThat(account.balance(), equalTo(amount));
    }

    @Test
    public void shouldWithdrawAmount() {
        Account account = new Account("1", "1");
        BigInteger depositAmount = BigInteger.TEN;
        BigInteger withdrawAmount = BigInteger.ONE;

        account.deposit(depositAmount);
        account.withdraw(withdrawAmount);

        assertThat(account.balance(), equalTo(depositAmount.subtract(withdrawAmount)));
    }
}
