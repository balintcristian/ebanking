package com.gitlab.balintcristian.ebanking.core.account;

import com.gitlab.balintcristian.ebanking.core.account.exceptions.InsufficientFunds;
import com.gitlab.balintcristian.ebanking.core.client.Client;
import com.gitlab.balintcristian.ebanking.core.client.ClientService;
import com.gitlab.balintcristian.ebanking.core.client.exceptions.ClientNotFound;
import com.gitlab.balintcristian.ebanking.core.transaction.Transaction;
import com.gitlab.balintcristian.ebanking.core.transaction.TransactionNumberGenerator;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigInteger;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.Mockito.*;

public class AccountServiceTest {

    @Mock
    private ClientService clientService;
    @Mock
    private AccountRepository accountRepository;
    @Mock
    private AccountNumberGenerator accountNumberGenerator;
    @Mock
    private TransactionNumberGenerator transactionNumberGenerator;

    private AccountService accountService;

    private ArgumentCaptor<Account> accountCaptor;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        accountCaptor = ArgumentCaptor.forClass(Account.class);

        accountService = new AccountService(clientService, accountRepository, accountNumberGenerator, transactionNumberGenerator);
    }

    @Test
    public void shouldOpenAccount() {
        when(clientService.exists("1")).thenReturn(true);
        when(clientService.getClient("1")).thenReturn(new Client("1", "John", "Doe"));

        Account account = accountService.openAccountFor("1");

        assertThat(account, notNullValue());
    }

    @Test(expected = ClientNotFound.class)
    public void shouldNotOpenAccountIfOwnerDoesNotExist() {
        when(clientService.exists("1")).thenReturn(false);

        accountService.openAccountFor("1");
    }

    @Test
    public void accountBalanceShouldBe_0_afterCreation() {
        when(clientService.exists("1")).thenReturn(true);
        when(clientService.getClient("1")).thenReturn(new Client("1", "John", "Doe"));

        Account account = accountService.openAccountFor("1");

        assertThat(account.balance(), is(BigInteger.ZERO));
    }

    @Test
    public void accountShouldHaveAccountNo() {
        when(clientService.exists("1")).thenReturn(true);
        when(clientService.getClient("1")).thenReturn(new Client("1", "John", "Doe"));
        when(accountNumberGenerator.next()).thenReturn("123");

        Account account = accountService.openAccountFor("1");

        assertThat(account.number(), is("123"));
    }

    @Test
    public void shouldSaveAccount() {
        when(clientService.exists("1")).thenReturn(true);
        when(clientService.getClient("1")).thenReturn(new Client("1", "John", "Doe"));
        when(accountNumberGenerator.next()).thenReturn("123");

        accountService.openAccountFor("1");

        verify(accountRepository).save(accountCaptor.capture());
        Account account = accountCaptor.getValue();
        assertThat(account.number(), is("123"));
        assertThat(account.ownerId(), is("1"));
        assertThat(account.balance(), is(BigInteger.ZERO));
    }


    @Test
    public void shouldTransferFunds() {
        BigInteger initialAmount = BigInteger.TEN;
        BigInteger transferAmount = BigInteger.ONE;
        Account accountTo = new Account("1", "123");
        Account accountFrom = new Account("2", "321");

        when(transactionNumberGenerator.next()).thenReturn("1");
        when(accountRepository.get("123")).thenReturn(accountTo);
        when(accountRepository.get("321")).thenReturn(accountFrom);

        accountService.deposit("321", initialAmount.toString());
        Transaction transfer = accountService.executeTransfer(accountFrom.number(), accountTo.number(), transferAmount.toString());

        verify(accountRepository, times(3)).save(accountCaptor.capture());
        Account savedAccountTo = accountCaptor.getAllValues().get(1);
        Account savedAccountFrom = accountCaptor.getAllValues().get(2);
        assertThat(transfer.id(), is("1"));
        assertThat(transfer.amount(), is(transferAmount));
        assertThat(savedAccountTo.balance(), is(transferAmount));
        assertThat(savedAccountFrom.balance(), is(initialAmount.subtract(transferAmount)));
    }

    @Test
    public void shouldDepositFunds() {
        BigInteger amount = BigInteger.TEN;
        Account account = new Account("1", "123");

        when(transactionNumberGenerator.next()).thenReturn("1");
        when(accountRepository.get(Matchers.any())).thenReturn(account);

        Transaction deposit = accountService.deposit("123", amount.toString());

        verify(accountRepository).save(accountCaptor.capture());
        Account savedAccount = accountCaptor.getValue();
        assertThat(deposit.id(), is("1"));
        assertThat(deposit.amount(), is(amount));
        assertThat(account.number(), is("123"));
        assertThat(account.ownerId(), is("1"));
        assertThat(savedAccount.balance(), is(amount));
    }

    @Test
    public void shouldWithdrawFunds() {
        BigInteger initialAmount = BigInteger.TEN;
        BigInteger withdrawAmount = BigInteger.TWO;
        Account account = new Account("1", "123");

        when(transactionNumberGenerator.next()).thenReturn("1");
        when(accountRepository.get(Matchers.any())).thenReturn(account);

        accountService.deposit("1", initialAmount.toString());
        Transaction withdraw = accountService.withdraw("1", withdrawAmount.toString());

        verify(accountRepository, times(2)).save(accountCaptor.capture());
        Account savedAccount = accountCaptor.getValue();
        assertThat(withdraw.id(), is("1"));
        assertThat(withdraw.amount(), is(withdrawAmount.negate()));
        assertThat(account.number(), is("123"));
        assertThat(account.ownerId(), is("1"));
        assertThat(savedAccount.balance(), is(initialAmount.subtract(withdrawAmount)));
    }


    @Test(expected = InsufficientFunds.class)
    public void shouldRejectWithdrawIfNoFunds() {
        BigInteger withdrawAmount = BigInteger.TWO;
        Account account = new Account("1", "123");

        when(accountRepository.get(Matchers.any())).thenReturn(account);

        accountService.withdraw("1", withdrawAmount.toString());
    }
}
