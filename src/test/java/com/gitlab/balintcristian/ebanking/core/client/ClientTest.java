package com.gitlab.balintcristian.ebanking.core.client;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class ClientTest {


    @Test
    public void shouldBeEqual() {
        Client client1 = new Client("1", "John", "Doe");
        Client client2 = new Client("1", "John", "Roe");
        Client client3 = new Client("2", "Jane", "Roe");
        Object object = new Object();

        assertEquals(client1, client2);
        assertNotEquals(client1, client3);
        assertNotEquals(client1, object);
    }
}
