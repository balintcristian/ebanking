package com.gitlab.balintcristian.ebanking.persistence.account;

import com.gitlab.balintcristian.ebanking.core.account.Account;
import com.gitlab.balintcristian.ebanking.core.client.Client;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class InMemoryAccountRepositoryTest {

    InMemoryAccountRepository accountRepository;

    @Before
    public void setUp() {
        accountRepository = new InMemoryAccountRepository();
    }

    @Test
    public void shouldSaveAccount() {
        Account account = new Account("1", "123");

        accountRepository.save(account);
        Account savedAccount = accountRepository.get("123");

        assertThat(savedAccount.number(), is(account.number()));
        assertThat(savedAccount.ownerId(), is(account.ownerId()));
        assertThat(savedAccount.balance(), is(account.balance()));
    }

    @Test
    public void shouldReturnAllAccounts() {
        Client owner = new Client("1", "John", "Doe");
        Account account1 = new Account("1", "123");
        Account account2 = new Account("2", "321");

        accountRepository.save(account1);
        accountRepository.save(account2);
        List<Account> accounts = accountRepository.getAll();

        assertThat(accounts.size(), is(2));
        assertThat(accounts.get(0), is(account1));
        assertThat(accounts.get(1), is(account2));
    }

    @Test
    public void shouldDeleteAccount() {
        Client owner = new Client("1", "John", "Doe");
        Account account1 = new Account("1", "123");
        Account account2 = new Account("2", "321");

        accountRepository.save(account1);
        accountRepository.save(account2);
        accountRepository.delete(account1);
        List<Account> accounts = accountRepository.getAll();

        assertThat(accounts.size(), is(1));
        assertThat(accounts.get(0), is(account2));
    }
}