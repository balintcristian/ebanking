package com.gitlab.balintcristian.ebanking.persistence.account;

import com.gitlab.balintcristian.ebanking.core.client.Client;
import com.gitlab.balintcristian.ebanking.persistence.client.InMemoryClientRepository;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class InMemoryClientRepositoryTest {

    InMemoryClientRepository clientRepository;

    @Before
    public void setUp() {
        clientRepository = new InMemoryClientRepository();
    }

    @Test
    public void shouldSaveClient() {
        Client owner = new Client("1", "John", "Doe");

        clientRepository.save(owner);
        Client savedOwner = clientRepository.get("1");

        assertThat(savedOwner.firstName(), is(owner.firstName()));
        assertThat(savedOwner.lastName(), is(owner.lastName()));
        assertThat(savedOwner.id(), is(owner.id()));
    }

    @Test
    public void shouldRetrieveAllClients() {
        Client owner1 = new Client("1", "John", "Doe");
        Client owner2 = new Client("2", "John", "Doe");

        clientRepository.save(owner1);
        clientRepository.save(owner2);
        List<Client> owners = clientRepository.getAll();

        assertThat(owners.size(), is(2));
        assertThat(owners.get(0), is(owner1));
        assertThat(owners.get(1), is(owner2));
    }

    @Test
    public void shouldDeleteClients() {
        Client owner1 = new Client("1", "John", "Doe");
        Client owner2 = new Client("2", "John", "Doe");

        clientRepository.save(owner1);
        clientRepository.save(owner2);
        clientRepository.delete(owner2);
        List<Client> owners = clientRepository.getAll();

        assertThat(owners.size(), is(1));
        assertThat(owners.get(0), is(owner1));
    }
}