package com.gitlab.balintcristian.ebanking.web.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreateClientDto {
    private String firstName;
    private String lastName;
}
