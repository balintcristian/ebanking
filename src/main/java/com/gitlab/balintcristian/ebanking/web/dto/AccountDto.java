package com.gitlab.balintcristian.ebanking.web.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.experimental.Accessors;

import java.util.List;

@Getter
@Builder
@Accessors(fluent = true)
public class AccountDto {
    Client owner;
    String number;
    String balance;
    List<TransactionDto> transactionHistory;

    @Builder
    final static class Client {
        private final String id;
        private final String firstName;
        private final String lastName;
    }
}
