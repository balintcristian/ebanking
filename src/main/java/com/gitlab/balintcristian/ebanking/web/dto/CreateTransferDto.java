package com.gitlab.balintcristian.ebanking.web.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreateTransferDto {
    private String to;
    private String from;
    private String amount;
}
