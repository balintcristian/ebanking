package com.gitlab.balintcristian.ebanking.web.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.experimental.Accessors;

import java.util.List;

@Getter
@Builder
@Accessors(fluent = true)
public class ClientDto {
    private final String id;
    private final String firstName;
    private final String lastName;
    private final List<Account> accounts;

    @Builder
    final static class Account {
        private final String number;
        private final String balance;
    }
}
