package com.gitlab.balintcristian.ebanking.web.controllers;

import com.gitlab.balintcristian.ebanking.core.account.AccountService;
import com.gitlab.balintcristian.ebanking.core.transaction.Transaction;
import com.gitlab.balintcristian.ebanking.web.dto.*;
import lombok.extern.slf4j.Slf4j;
import spark.Request;
import spark.Response;

import static com.gitlab.balintcristian.ebanking.web.Configuration.GSON;

@Slf4j
public class TransactionController {

    private final DtoMapper dtoMapper;
    private final AccountService accountService;

    public TransactionController(DtoMapper dtoMapper, AccountService accountService) {
        this.dtoMapper = dtoMapper;
        this.accountService = accountService;
    }

    public TransactionDto transferTo(Request request, Response response) {
        CreateTransferDto transferDto = GSON.fromJson(request.body(), CreateTransferDto.class);
        log.info("transaction from: '{}', account: '{}', amount: {}", transferDto.getFrom(), transferDto.getTo(), transferDto.getAmount());

        Transaction transaction = accountService.executeTransfer(transferDto.getFrom(), transferDto.getTo(), transferDto.getAmount());

        return dtoMapper.from(transaction);
    }

    public TransactionDto deposit(Request request, Response response) {
        CreateDepositDto depositDto = GSON.fromJson(request.body(), CreateDepositDto.class);
        log.info("deposit {} account account '{}'", depositDto.getAmount(), depositDto.getTo());

        Transaction transaction = accountService.deposit(depositDto.getTo(), depositDto.getAmount());

        return dtoMapper.from(transaction);
    }

    public TransactionDto withdraw(Request request, Response response) {
        CreateWithdrawDto withdrawDto = GSON.fromJson(request.body(), CreateWithdrawDto.class);
        log.info("withdraw {} from account '{}'", withdrawDto.getAmount(), withdrawDto.getFrom());

        Transaction transaction = accountService.withdraw(withdrawDto.getFrom(), withdrawDto.getAmount());

        return dtoMapper.from(transaction);
    }
}
