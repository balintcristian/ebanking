package com.gitlab.balintcristian.ebanking.web.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.experimental.Accessors;

@Getter
@Builder
@Accessors(fluent = true)
public class TransactionDto {
    private final String id;
    private final String amount;
}
