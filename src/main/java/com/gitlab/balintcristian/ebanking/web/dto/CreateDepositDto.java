package com.gitlab.balintcristian.ebanking.web.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreateDepositDto {
    private String to;
    private String amount;
}
