package com.gitlab.balintcristian.ebanking.web;

import com.gitlab.balintcristian.ebanking.core.account.AccountService;
import com.gitlab.balintcristian.ebanking.core.client.ClientService;
import com.gitlab.balintcristian.ebanking.web.controllers.AccountController;
import com.gitlab.balintcristian.ebanking.web.controllers.ClientController;
import com.gitlab.balintcristian.ebanking.web.controllers.TransactionController;
import com.gitlab.balintcristian.ebanking.web.dto.DtoMapper;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.jetty.http.HttpStatus;
import spark.Filter;
import spark.ResponseTransformer;
import spark.Spark;

import static spark.Spark.*;

@Slf4j
public class Configuration {
    public static final Gson GSON = new Gson();

    private static final ResponseTransformer JSON_TRANSFORMER = GSON::toJson;

    private static final Filter JSON_CONTENT_TYPE_FILTER =
            (req, res) -> res.header("Content-Type", "application/json");
    private static final Filter LOG_INCOMING_REQUEST_FILTER = (req, ignored) ->
            log.info(">>> {} {}", req.requestMethod(), req.pathInfo());
    private Filter LOG_OUTGOING_RESPONSE_FILTER = (ignored, res) ->
            log.info("<<< {} {}", res.status(), HttpStatus.getMessage(res.status()));

    private final ClientController clientController;
    private final AccountController accountController;
    private final TransactionController transactionController;

    public Configuration(ClientService clientService, AccountService accountService) {
        DtoMapper dtoMapper = new DtoMapper(clientService, accountService);
        clientController = new ClientController(dtoMapper, clientService);
        accountController = new AccountController(dtoMapper, accountService);
        transactionController = new TransactionController(dtoMapper, accountService);
    }

    public void start() {
        port(8080);

        before(LOG_INCOMING_REQUEST_FILTER);

        get("/clients/", clientController::getClients, JSON_TRANSFORMER);
        get("/clients/:clientId", clientController::getClient, JSON_TRANSFORMER);
        post("/clients/", "application/json", clientController::createClient, JSON_TRANSFORMER);

        get("/accounts/", accountController::getAccounts, JSON_TRANSFORMER);
        get("/accounts/:accountNo", accountController::getAccount, JSON_TRANSFORMER);
        post("/accounts/", "application/json", accountController::createAccount, JSON_TRANSFORMER);

        post("/transactions/deposit", "application/json", transactionController::deposit, JSON_TRANSFORMER);
        post("/transactions/withdraw", "application/json", transactionController::withdraw, JSON_TRANSFORMER);
        post("/transactions/transfer", "application/json", transactionController::transferTo, JSON_TRANSFORMER);

        after(JSON_CONTENT_TYPE_FILTER);
        after(LOG_OUTGOING_RESPONSE_FILTER);
    }

    public void stop() {
        Spark.stop();
    }
}
