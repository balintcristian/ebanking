package com.gitlab.balintcristian.ebanking.web.controllers;


import com.gitlab.balintcristian.ebanking.core.account.Account;
import com.gitlab.balintcristian.ebanking.core.account.AccountService;
import com.gitlab.balintcristian.ebanking.core.transaction.Transaction;
import com.gitlab.balintcristian.ebanking.web.dto.*;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.jetty.http.HttpStatus;
import spark.Request;
import spark.Response;

import java.util.List;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;

import static com.gitlab.balintcristian.ebanking.web.Configuration.GSON;

@Slf4j
public class AccountController {

    private final static BiConsumer<Object, Response> NOT_FOUND_HANDLER = (t, response) -> {
        if(t == null) {
            response.status(HttpStatus.NOT_FOUND_404);
        }
    };

    private final DtoMapper dtoMapper;
    private final AccountService accountService;

    public AccountController(DtoMapper dtoMapper, AccountService accountService) {
        this.dtoMapper = dtoMapper;
        this.accountService = accountService;
    }

    public AccountDto getAccount(Request request, Response response) {
        String accountNo = request.params("accountNo");
        log.info("retrieve account: '{}'", accountNo);

        Account account = accountService.getAccount(accountNo);

        NOT_FOUND_HANDLER.accept(account, response);

        return dtoMapper.from(account);
    }

    public List<AccountDto> getAccounts(Request request, Response response) {
        log.info("retrieve accounts");
        return accountService.getAccounts().stream()
                .map(dtoMapper::from)
                .collect(Collectors.toList());
    }

    public AccountDto createAccount(Request request, Response response) {
        CreateAccountDto createAccountDto = GSON.fromJson(request.body(), CreateAccountDto.class);
        Account account = accountService.openAccountFor(createAccountDto.getClientId());
        return dtoMapper.from(account);
    }
}
