package com.gitlab.balintcristian.ebanking.web.dto;

import com.gitlab.balintcristian.ebanking.core.account.Account;
import com.gitlab.balintcristian.ebanking.core.account.AccountService;
import com.gitlab.balintcristian.ebanking.core.client.Client;
import com.gitlab.balintcristian.ebanking.core.client.ClientService;
import com.gitlab.balintcristian.ebanking.core.transaction.Transaction;

import java.util.List;
import java.util.stream.Collectors;

public class DtoMapper {

    private final ClientService clientService;
    private final AccountService accountService;

    public DtoMapper(ClientService clientService, AccountService accountService) {
        this.clientService = clientService;
        this.accountService = accountService;
    }

    public ClientDto from(Client client) {
        List<Account> accounts = client.accounts().stream()
                .map(accountService::getAccount)
                .collect(Collectors.toList());
        return ClientDto.builder()
                .id(client.id())
                .lastName(client.lastName())
                .firstName(client.firstName())
                .accounts(accounts.stream()
                        .map(account -> ClientDto.Account.builder()
                                .number(account.number())
                                .balance(account.balance().toString())
                                .build())
                        .collect(Collectors.toList()))
                .build();
    }

    public AccountDto from(Account account) {
        Client owner = clientService.getClient(account.ownerId());
        return AccountDto.builder()
                .number(account.number())
                .balance(account.balance().toString())
                .owner(AccountDto.Client.builder()
                        .id(owner.id())
                        .lastName(owner.lastName())
                        .firstName(owner.firstName())
                        .build())
                .transactionHistory(account.transactions().stream()
                        .map(transaction -> TransactionDto.builder()
                                .id(transaction.id())
                                .amount(transaction.amount().toString())
                                .build())
                        .collect(Collectors.toList()))
                .build();
    }

    public TransactionDto from(Transaction transaction) {
        return TransactionDto.builder()
                .id(transaction.id())
                .amount(transaction.amount().toString())
                .build();
    }
}
