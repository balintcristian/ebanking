package com.gitlab.balintcristian.ebanking.web.controllers;

import com.gitlab.balintcristian.ebanking.core.client.Client;
import com.gitlab.balintcristian.ebanking.core.client.ClientService;
import com.gitlab.balintcristian.ebanking.web.dto.ClientDto;
import com.gitlab.balintcristian.ebanking.web.dto.CreateClientDto;
import com.gitlab.balintcristian.ebanking.web.dto.DtoMapper;
import lombok.extern.slf4j.Slf4j;
import spark.Request;
import spark.Response;

import java.util.List;
import java.util.stream.Collectors;

import static com.gitlab.balintcristian.ebanking.web.Configuration.GSON;

@Slf4j
public class ClientController {

    private final DtoMapper dtoMapper;
    private final ClientService clientService;

    public ClientController(DtoMapper dtoMapper, ClientService clientService) {
        this.dtoMapper = dtoMapper;
        this.clientService = clientService;
    }

    public ClientDto getClient(Request request, Response response) {
        String clientId = request.params("clientId");
        log.info("retrieve client: '{}'", clientId);

        Client client = clientService.getClient(clientId);

        return dtoMapper.from(client);
    }

    public List<ClientDto> getClients(Request request, Response response) {
        return clientService.getClients().stream()
                .map(dtoMapper::from)
                .collect(Collectors.toList());
    }

    public ClientDto createClient(Request request, Response response) {
        CreateClientDto createClientDto = GSON.fromJson(request.body(), CreateClientDto.class);
        Client client = clientService.createClient(createClientDto.getFirstName(), createClientDto.getLastName());
        return dtoMapper.from(client);
    }
}
