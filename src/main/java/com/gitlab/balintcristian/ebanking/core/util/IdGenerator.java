package com.gitlab.balintcristian.ebanking.core.util;

public interface IdGenerator {
    String nextId();
}
