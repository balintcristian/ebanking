package com.gitlab.balintcristian.ebanking.core.transaction;

import java.util.concurrent.atomic.AtomicLong;

public class SequentialTransactionNumberGenerator implements TransactionNumberGenerator {
    private static AtomicLong transactionId = new AtomicLong(1);

    @Override
    public String next() {
        return String.valueOf(transactionId.getAndIncrement());
    }
}
