package com.gitlab.balintcristian.ebanking.core.account;

import com.gitlab.balintcristian.ebanking.core.transaction.Transaction;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Account {
    private final String number;
    private final String ownerId;

    private BigInteger balance;
    private List<Transaction> transactions;

    public Account(String ownerId, String number) {
        this.ownerId = ownerId;
        this.number = number;
        this.balance = BigInteger.ZERO;
        this.transactions = new ArrayList<>();
    }

    public synchronized void deposit(BigInteger amount) {
        balance = balance.add(amount);
    }

    public synchronized void withdraw(BigInteger amount) {
        balance = balance.subtract(amount);
    }

    public String number() {
        return number;
    }

    public String ownerId() {
        return ownerId;
    }

    public BigInteger balance() {
        return balance;
    }

    public void addTransaction(Transaction transaction) {
        transactions.add(transaction);
    }

    public List<Transaction> transactions() {
        return transactions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return Objects.equals(number, account.number);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number);
    }
}
