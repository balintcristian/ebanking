package com.gitlab.balintcristian.ebanking.core.account.exceptions;

public class InsufficientFunds extends RuntimeException {
    public InsufficientFunds(String message, Object... params) {
        super(String.format(message.replace("{}", "%s"), params));
    }
}
