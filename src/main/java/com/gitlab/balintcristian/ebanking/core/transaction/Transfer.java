package com.gitlab.balintcristian.ebanking.core.transaction;

import com.gitlab.balintcristian.ebanking.core.account.Account;

import java.math.BigInteger;

public class Transfer implements Transaction {

    private final String id;
    private final Account to;
    private final Account from;
    private final BigInteger amount;

    public Transfer(String id, Account to, Account from, String amount) {
        this.id = id;
        this.to = to;
        this.from = from;
        this.amount = new BigInteger(amount);
    }

    @Override
    public String id() {
        return id;
    }

    @Override
    public synchronized void  execute() {
        to.deposit(amount);
        from.withdraw(amount);
        to.addTransaction(this);
        from.addTransaction(this);
    }

    @Override
    public BigInteger amount() {
        return amount;
    }
}
