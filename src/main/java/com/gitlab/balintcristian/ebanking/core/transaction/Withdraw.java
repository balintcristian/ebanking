package com.gitlab.balintcristian.ebanking.core.transaction;

import com.gitlab.balintcristian.ebanking.core.account.Account;
import com.gitlab.balintcristian.ebanking.core.account.exceptions.InsufficientFunds;

import java.math.BigInteger;

public class Withdraw implements Transaction {

    private final String id;
    private final Account from;
    private final BigInteger amount;

    public Withdraw(String id, Account from, String amount) {
        this.id = id;
        this.from = from;
        this.amount = new BigInteger(amount);
    }

    @Override
    public String id() {
        return id;
    }

    @Override
    public synchronized void execute() {
        if (from.balance().compareTo(amount) < 0)
            throw new InsufficientFunds("account '{}' has insufficient funds account execute transaction '{}'", from.number(), id);

        from.withdraw(amount);
        from.addTransaction(this);
    }

    @Override
    public BigInteger amount() {
        return amount.negate();
    }
}
