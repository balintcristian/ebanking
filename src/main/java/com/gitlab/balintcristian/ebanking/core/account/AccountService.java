package com.gitlab.balintcristian.ebanking.core.account;

import com.gitlab.balintcristian.ebanking.core.client.Client;
import com.gitlab.balintcristian.ebanking.core.client.ClientService;
import com.gitlab.balintcristian.ebanking.core.client.exceptions.ClientNotFound;
import com.gitlab.balintcristian.ebanking.core.transaction.*;

import java.util.List;

public class AccountService {

    private final AccountNumberGenerator accountNumberGenerator;
    private final TransactionNumberGenerator transactionNumberGenerator;

    private final ClientService clientService;
    private final AccountRepository accountRepository;

    public AccountService(
            ClientService clientService,
            AccountRepository accountRepository,
            AccountNumberGenerator accountNumberGenerator,
            TransactionNumberGenerator transactionNumberGenerator
    ) {
        this.clientService = clientService;
        this.accountRepository = accountRepository;
        this.accountNumberGenerator = accountNumberGenerator;
        this.transactionNumberGenerator = transactionNumberGenerator;
    }

    public Account openAccountFor(String clientId) {
        if (!clientService.exists(clientId))
            throw new ClientNotFound("client with id '{}' not found.", clientId);

        Client client = clientService.getClient(clientId);
        Account account = new Account(clientId, accountNumberGenerator.next());
        accountRepository.save(account);
        client.registerAccount(account.number());

        return account;
    }

    public Transaction executeTransfer(String from, String to, String amount) {
        Account toAccount = accountRepository.get(to);
        Account fromAccount = accountRepository.get(from);

        Transfer transfer = new Transfer(transactionNumberGenerator.next(), toAccount, fromAccount, amount);

        transfer.execute();
        accountRepository.save(toAccount);
        accountRepository.save(fromAccount);
        return transfer;

    }

    public Transaction deposit(String accountNo, String amount) {
        Account toAccount = accountRepository.get(accountNo);
        Deposit deposit = new Deposit(transactionNumberGenerator.next(), toAccount, amount);

        deposit.execute();
        accountRepository.save(toAccount);
        return deposit;
    }

    public Transaction withdraw(String accountNo, String amount) {
        Account fromAccount = accountRepository.get(accountNo);
        Withdraw withdraw = new Withdraw(transactionNumberGenerator.next(), fromAccount, amount);
        withdraw.execute();
        accountRepository.save(fromAccount);
        return withdraw;
    }

    public Account getAccount(String accountNo) {
        return accountRepository.get(accountNo);
    }

    public List<Account> getAccounts() {
        return accountRepository.getAll();
    }
}
