package com.gitlab.balintcristian.ebanking.core.account;

@FunctionalInterface
public interface AccountNumberGenerator {

    String next();
}
