package com.gitlab.balintcristian.ebanking.core.transaction;

import com.gitlab.balintcristian.ebanking.core.account.Account;

import java.math.BigInteger;

public class Deposit implements Transaction {

    private final String id;
    private final Account to;
    private final BigInteger amount;

    public Deposit(String id, Account to, String amount) {
        this.id = id;
        this.to = to;
        this.amount = new BigInteger(amount);
    }

    @Override
    public String id() {
        return id;
    }

    @Override
    public synchronized void execute() {
        to.deposit(amount);
        to.addTransaction(this);
    }

    @Override
    public BigInteger amount() {
        return amount;
    }
}
