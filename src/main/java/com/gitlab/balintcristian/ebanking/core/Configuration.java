package com.gitlab.balintcristian.ebanking.core;

import com.gitlab.balintcristian.ebanking.core.account.*;
import com.gitlab.balintcristian.ebanking.core.account.AccountRepository;
import com.gitlab.balintcristian.ebanking.core.client.ClientRepository;
import com.gitlab.balintcristian.ebanking.core.client.ClientService;
import com.gitlab.balintcristian.ebanking.core.transaction.SequentialTransactionNumberGenerator;
import com.gitlab.balintcristian.ebanking.core.util.IdGenerator;

public class Configuration {

    private final ClientService clientService;
    private final AccountService accountService;

    public Configuration(
            IdGenerator idGenerator,
            ClientRepository clientRepository,
            AccountRepository accountRepository
    ) {
        this.clientService = new ClientService(idGenerator, clientRepository);
        this.accountService = new AccountService(
                clientService,
                accountRepository,
                new GbAccountNumberGenerator(),
                new SequentialTransactionNumberGenerator()
        );
    }

    public AccountService accountService() {
        return accountService;
    }

    public ClientService clientService() {
        return clientService;
    }
}
