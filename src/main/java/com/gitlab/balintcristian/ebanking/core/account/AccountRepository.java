package com.gitlab.balintcristian.ebanking.core.account;

import com.gitlab.balintcristian.ebanking.core.account.Account;

import java.util.List;

public interface AccountRepository {
    void save(Account capture);

    void delete(Account account);

    Account get(String accountNo);

    List<Account> getAll();
}
