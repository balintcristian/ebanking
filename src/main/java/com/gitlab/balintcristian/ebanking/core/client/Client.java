package com.gitlab.balintcristian.ebanking.core.client;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Client {
    private final String id;
    private final String lastName;
    private final String firstName;
    private final List<String> accounts;

    public Client(String id, String firstName, String lastName) {
        this.id = id;
        this.lastName = lastName;
        this.firstName = firstName;
        this.accounts = new ArrayList<>();
    }

    public void registerAccount(String accountId) {
        accounts.add(accountId);
    }

    public String id() {
        return id;
    }

    public String lastName() {
        return lastName;
    }

    public String firstName() {
        return firstName;
    }

    public List<String> accounts() {
        return accounts;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Client that = (Client) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
