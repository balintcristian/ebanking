package com.gitlab.balintcristian.ebanking.core.transaction;

public interface TransactionNumberGenerator {
    String next();
}
