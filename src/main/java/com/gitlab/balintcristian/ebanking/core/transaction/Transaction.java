package com.gitlab.balintcristian.ebanking.core.transaction;

import java.math.BigInteger;

public interface Transaction {

    String id();
    BigInteger amount();

    void execute();
}
