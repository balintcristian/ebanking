package com.gitlab.balintcristian.ebanking.core.client;

import com.gitlab.balintcristian.ebanking.core.client.Client;

import java.util.List;

public interface ClientRepository {
    void save(Client capture);

    void delete(Client owner);

    Client get(String id);

    List<Client> getAll();
}
