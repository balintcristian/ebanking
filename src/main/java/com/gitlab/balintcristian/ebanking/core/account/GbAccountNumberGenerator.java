package com.gitlab.balintcristian.ebanking.core.account;

import java.util.Random;
import java.util.stream.Collectors;

public class GbAccountNumberGenerator implements AccountNumberGenerator {

    private static final String COUNTRY_CODE = "GB";
    private static final String CHECK_DIGITS = "29";

    private static Random random;

    public GbAccountNumberGenerator() {
        random = new Random(System.currentTimeMillis());
    }

    public String next() {

        String BBAN = random.ints(14, 0, 10)
                .boxed()
                .map(Object::toString)
                .collect(Collectors.joining());

        return String.format("%s%s%s", COUNTRY_CODE, CHECK_DIGITS, BBAN);
    }
}
