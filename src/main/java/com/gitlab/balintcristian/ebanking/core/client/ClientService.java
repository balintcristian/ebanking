package com.gitlab.balintcristian.ebanking.core.client;

import com.gitlab.balintcristian.ebanking.core.util.IdGenerator;

import java.util.List;
import java.util.Optional;

public class ClientService {

    private final IdGenerator idGenerator;
    private final ClientRepository clientRepository;

    public ClientService(IdGenerator idGenerator, ClientRepository clientRepository) {
        this.idGenerator = idGenerator;
        this.clientRepository = clientRepository;
    }

    public Client createClient(String firstName, String lastName) {
        Client client = new Client(idGenerator.nextId(), firstName, lastName);
        clientRepository.save(client);
        return client;
    }

    public Client getClient(String clientId) {
        return clientRepository.get(clientId);
    }

    public List<Client> getClients() {
        return clientRepository.getAll();
    }

    public boolean exists(String clientId) {
        return Optional.ofNullable(clientId)
                .isPresent();
    }
}
