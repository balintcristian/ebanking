package com.gitlab.balintcristian.ebanking.core.client.exceptions;

public class ClientNotFound extends RuntimeException {
    public ClientNotFound(String message, Object... variables) {
        super(String.format(message.replace("{}", "%s"), variables));
    }
}
