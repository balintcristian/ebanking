package com.gitlab.balintcristian.ebanking.persistence.account;

import com.gitlab.balintcristian.ebanking.core.account.Account;
import com.gitlab.balintcristian.ebanking.core.account.AccountRepository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InMemoryAccountRepository implements AccountRepository {

    private Map<String, Account> accounts = new HashMap<>();

    @Override
    public void save(Account account) {
        accounts.put(account.number(), account);
    }

    @Override
    public void delete(Account account) {
        accounts.remove(account.number());
    }

    @Override
    public Account get(String accountNo) {
        return accounts.get(accountNo);
    }

    @Override
    public List<Account> getAll() {
        return new ArrayList<>(accounts.values());
    }
}
