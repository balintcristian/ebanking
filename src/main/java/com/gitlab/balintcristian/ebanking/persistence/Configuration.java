package com.gitlab.balintcristian.ebanking.persistence;

import com.gitlab.balintcristian.ebanking.core.client.ClientRepository;
import com.gitlab.balintcristian.ebanking.core.account.AccountRepository;
import com.gitlab.balintcristian.ebanking.persistence.client.InMemoryClientRepository;
import com.gitlab.balintcristian.ebanking.persistence.account.InMemoryAccountRepository;

public class Configuration {

    AccountRepository accountRepository = new InMemoryAccountRepository();
    ClientRepository clientRepository = new InMemoryClientRepository();

    public AccountRepository accountRepository() {
        return accountRepository;
    }

    public ClientRepository clientRepository() {
        return clientRepository;
    }
}
