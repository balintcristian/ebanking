package com.gitlab.balintcristian.ebanking.persistence.client;

import com.gitlab.balintcristian.ebanking.core.client.Client;
import com.gitlab.balintcristian.ebanking.core.client.ClientRepository;

import java.util.*;

public class InMemoryClientRepository implements ClientRepository {

    private Map<String, Client> owners = new HashMap<>();

    @Override
    public void save(Client owner) {
        owners.put(owner.id(), owner);
    }

    @Override
    public void delete(Client owner) {
        owners.remove(owner.id());
    }

    @Override
    public Client get(String id) {
        return owners.get(id);
    }

    @Override
    public List<Client> getAll() {
        return new ArrayList<>(owners.values());
    }
}
