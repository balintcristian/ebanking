package com.gitlab.balintcristian.ebanking;

import com.gitlab.balintcristian.ebanking.core.client.Client;
import com.gitlab.balintcristian.ebanking.core.util.IdGenerator;

import java.util.concurrent.atomic.AtomicLong;

public class Application {

    private static AtomicLong clientIndex = new AtomicLong(1);
    private static IdGenerator clientIdGenerator = () -> String.valueOf(clientIndex.getAndIncrement());

    private static com.gitlab.balintcristian.ebanking.web.Configuration webConfiguration;
    private static com.gitlab.balintcristian.ebanking.core.Configuration coreCoreConfiguration;
    private static com.gitlab.balintcristian.ebanking.persistence.Configuration persistenceConfiguration;

    public static void main(String[] args) {
        start();
        loadMockData();
    }

    private static void init() {
        persistenceConfiguration = new com.gitlab.balintcristian.ebanking.persistence.Configuration();
        coreCoreConfiguration = new com.gitlab.balintcristian.ebanking.core.Configuration(
                clientIdGenerator,
                persistenceConfiguration.clientRepository(),
                persistenceConfiguration.accountRepository()
        );
        webConfiguration = new com.gitlab.balintcristian.ebanking.web.Configuration(
                coreCoreConfiguration.clientService(),
                coreCoreConfiguration.accountService()
        );
    }

    private static void loadMockData() {
        Client john = new Client(clientIdGenerator.nextId(), "John", "Doe");
        persistenceConfiguration.clientRepository().save(john);
        coreCoreConfiguration.accountService().openAccountFor(john.id());
        coreCoreConfiguration.accountService().openAccountFor(john.id());

        Client jane = new Client(clientIdGenerator.nextId(), "Jane", "Roe");
        persistenceConfiguration.clientRepository().save(jane);
        coreCoreConfiguration.accountService().openAccountFor(jane.id());
    }

    public static void start() {
        init();
        webConfiguration.start();
    }

    public static void stop() {
        webConfiguration.stop();
    }

    public static com.gitlab.balintcristian.ebanking.persistence.Configuration persistenceConfiguration() {
        return persistenceConfiguration;
    }
}
